function oStack = vertcat(varargin)

% vertcat - METHOD Overloaded vertcat method

oStack = cat(nan, varargin{:});

% --- END of vertcat METHOD ---
